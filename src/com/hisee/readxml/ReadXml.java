package com.hisee.readxml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.hisee.util.FileUtil;

public class ReadXml {
	/**
	 * xml内容转为Document
	 * @param content xml内容
	 * @return 
	 * @throws DocumentException
	 */
	private static Document getDocumentByString(String content) throws DocumentException 
	{
		SAXReader reader = new SAXReader();
		InputStream is = new ByteArrayInputStream(content.getBytes());
		Document document =reader.read(is);
		return document;
	}
	
	/**
	 * 读取文本或字符串的内容
	 * 如果只要读字符串，将
	 * String content =getFile2String(path);
	 * 指定xml格式字符串即可
	 * @param message 消息正文
	 * @return Map<String,Object> 待插入的值
	 * @throws DocumentException
	 */
	@SuppressWarnings("unchecked")
	public static Map<String,Object> parseApplyXmlToMap(String message) throws DocumentException  {
		
		Map<String,Object> retMap=new HashMap<String,Object>();
		String content = message;
		Document document =getDocumentByString(content);
		retMap.put("request_data", content);
//		Document document = reader.read(file);
		Element root = document.getRootElement();
//		examination_code 申请id
		Element id=root.element("id");
		System.out.println("申请:==>"+id.attributeValue("extension"));
//		retMap.put("examination_code", id.attributeValue("extension"));
		
		Element placerGroup=root.element("controlActProcess").element("subject").element("placerGroup");
		
		Element assignedEntity= placerGroup.element("author").element("assignedEntity");
//		apply_doctor_code 开单医生编码
		Element apply_doctor_code=assignedEntity.element("id").element("item");
		System.out.println("开单医生编码:==>"+apply_doctor_code.attributeValue("extension"));
		retMap.put("apply_doctor_code", apply_doctor_code.attributeValue("extension"));
		
//		apply_doctor_name 开单医生姓名
		Element apply_doctor_name=assignedEntity.element("assignedPerson").element("name").element("item").element("part");
		System.out.println("开单医生姓名:==>"+apply_doctor_name.attributeValue("value"));
		retMap.put("apply_doctor_name", apply_doctor_name.attributeValue("value"));
		
//		apply_dept_code 申请科室编码
		Element app_dept_code= assignedEntity.element("representedOrganization").element("id").element("item");
		System.out.println("申请科室编码:==>"+app_dept_code.attributeValue("extension"));
		retMap.put("apply_dept_code", app_dept_code.attributeValue("extension"));
		
		
//		apply_dept_name 申请科室名称
		Element app_dept_name= assignedEntity.element("representedOrganization").element("name").element("item").element("part");
		System.out.println("申请科室名称:==>"+app_dept_name.attributeValue("value"));
		retMap.put("apply_dept_name", app_dept_name.attributeValue("value"));
		
//		patient_code 患者ID（来自XML接口数据的患者ID）
		Iterator<Element> patient_code =placerGroup.element("subject").element("patient").element("id").elementIterator("item");
		patient_code.next();
		String patient_code_ =patient_code.next().attributeValue("extension");
		System.out.println("患者ID:==>"+patient_code_);
		retMap.put("patient_code", patient_code_);
		
		String visit_no =patient_code.next().attributeValue("extension");
		System.out.println("就诊号:==>"+visit_no);
		retMap.put("visit_no", visit_no);
		
		Element patientPerson= placerGroup.element("subject").element("patient").element("patientPerson");
//		patient_name patientPerson->姓名
		Element patient_name =patientPerson.element("name").element("item").element("part");
		System.out.println("患者姓名:==>"+patient_name.attributeValue("value"));
		retMap.put("patient_name", patient_name.attributeValue("value"));
		
//		card_no 身份证号
		Iterator<Element> card_no_idcard_hos =patientPerson.element("id").elementIterator("item");
		String card_no =card_no_idcard_hos.next().attributeValue("extension");
		System.out.println("身份证号:==>"+card_no);
		retMap.put("card_no", card_no);
		
//		idcard_hos 医保卡号		
		String idcard_hos=card_no_idcard_hos.next().attributeValue("extension");
		System.out.println("医保卡号:==>"+idcard_hos);
		retMap.put("idcard_hos", idcard_hos);
		
//		sex 性别代码
		Element sex =patientPerson.element("administrativeGenderCode"); 
		System.out.println("性别代码:==>"+sex.attributeValue("code"));
		String strGender = "女";
		if (sex.attributeValue("code").equals("1")) {
			strGender = "男";
		}
		retMap.put("sex", strGender);
		
//		age 年龄
		Element age =patientPerson.element("birthTime").element("originalText");
		System.out.println("年龄:==>"+age.attributeValue("value"));
		retMap.put("age", age.attributeValue("value"));
		
//		phone 联系电话
		Element phone =patientPerson.element("telecom").element("item");
		System.out.println("联系电话:==>"+phone.attributeValue("value"));
		retMap.put("phone", phone.attributeValue("value"));
		
//		hospitalization_area 病区编码/病区名（BAG_AD->BNR）
		
		Iterator<Element> hospitalization_area_bed_code =placerGroup.element("subject").element("patient").element("addr").element("item").elementIterator("part");
		String hospitalization_area =hospitalization_area_bed_code.next().attributeValue("value");
		System.out.println("病区编码/病区名:==>"+hospitalization_area);
		retMap.put("hospitalization_area", hospitalization_area);
		
//		bed_code 床号（BAG_AD->BNR）
		String bed_code =hospitalization_area_bed_code.next().attributeValue("value");
		System.out.println("床号:==>"+bed_code);
		retMap.put("bed_code", bed_code);
		
		Element component2 =placerGroup.element("component2");
//		examination_code 检查申请单编号
		Element check_id=component2.element("observationRequest").element("id").element("item");
		System.out.println("检查申请单编号:==>"+check_id.attributeValue("extension"));
		retMap.put("examination_code", check_id.attributeValue("extension"));
			
//		hospitalization_apply_time 检查申请日期
		Element hospitalization_apply_time=component2.element("observationRequest").element("effectiveTime").element("any");
		System.out.println("检查申请日期:==>"+hospitalization_apply_time.attributeValue("value"));
		retMap.put("hospitalization_apply_time", hospitalization_apply_time.attributeValue("value"));

//		extend 医嘱号
		Element extend =component2.element("observationRequest").element("component2").element("observationRequest").element("id").element("item");
		System.out.println("医嘱号:==>"+extend.attributeValue("extension"));
		retMap.put("doctor_advice_id", extend.attributeValue("extension"));

		//此处值固定,值待改？
		retMap.put("hospital_id", 1);
		return retMap;
	}
	
	/**
	 * 读取examination_code 是BS002（开单）或 BS005(撤单)
	 * 
	 * @param message
	 *            文件路径
	 * @return  
	 * @throws DocumentException
	 */
	public static Map<String, Object> withdrawToMap(String message)
			throws DocumentException {

		Map<String, Object> retMap = new HashMap<String, Object>();
		Document document = getDocumentByString(message);
		retMap.put("request_data", message);
		Element root = document.getRootElement();
		// examination_code 申请id
		Element id = root.element("id");
		retMap.put("examination_code", id.attributeValue("extension"));
		return retMap;
	}
	
	/**
	 * 取出BS00x来判断是开单还是撤单
	 * 
	 * @return 返回BS002(开单)或BS005(撤单)
	 * @throws DocumentException
	 */
	public static String getRequestType(String message) throws DocumentException {
		Map<String, Object> map = withdrawToMap(message);
		Object obj = map.get("examination_code");
		if (obj != null) {
			return map.get("examination_code").toString();
		}
		return null;
	}

	/**
	 * 取出医嘱号
	 *
	 * @return
	 * @throws DocumentException
	 */
	public static String getDoctorAdviceId(String message) throws DocumentException {
		Map<String, Object> map = withdrawToMap2(message);
		Object obj = map.get("doctor_advice_id");
		if (obj != null) {
			return map.get("doctor_advice_id").toString();
		}
		return null;
	}

	/**
	 * 读取doctor_advice_id医嘱编号
	 *
	 * @param message
	 *            消息正文
	 * @return
	 * @throws DocumentException
	 */
	public static Map<String, Object> withdrawToMap2(String message)
			throws DocumentException {
		Map<String, Object> retMap = new HashMap<String, Object>();
		Document document = getDocumentByString(message);
		retMap.put("request_data", message);
		Element root = document.getRootElement();
		// examination_code 申请id
		Element id = root.element("controlActProcess").element("subject").element("placerGroup")
				.element("component2").element("substanceAdministrationRequest").element("id");
		retMap.put("doctor_advice_id", id.attributeValue("extension"));
		return retMap;
	}
}
