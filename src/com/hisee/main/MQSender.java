package com.hisee.main;

import com.ibm.mq.*;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.constants.MQConstants;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Hashtable;

import com.hisee.util.SimpleStringUtil2;

public class MQSender {

    private static Hashtable<String, Comparable> env = new Hashtable<>();

    private static MQQueueManager mqQueueManager;

    private static MQQueue mqQueue;

    private static final String QUEUE_MANAGEMENT_NAME = "GWI.QM";

    private static final String QUEUE_NAME = "IN.BS004.LQ";

    private static final String HOST = "194.1.14.132";

    private static final String CHANNEL = "IE.SVRCONN";

    private static final int PORT = 5000;

    private static final int CCID = 1208;

    static {
        // 设置队列管理器名称
        env.put(CMQC.HOST_NAME_PROPERTY, HOST);
        // 设置频道
        env.put(CMQC.CHANNEL_PROPERTY, CHANNEL);
        // 设置CCID
        env.put(CMQC.CCSID_PROPERTY, CCID);
        // 设置端口
        env.put(CMQC.PORT_PROPERTY, PORT);
        // 设置transport
        env.put(CMQC.TRANSPORT_PROPERTY, CMQC.TRANSPORT_MQSERIES);
    }

    private synchronized static void connectMQ() throws MQException {
        if (mqQueueManager == null)
            mqQueueManager = new MQQueueManager(QUEUE_MANAGEMENT_NAME, env);
    }

    private synchronized static void testCreateQueue() {
        // 队列打开参数
        int openOptions = MQConstants.MQOO_BIND_AS_Q_DEF | MQConstants.MQOO_OUTPUT;

        try {
            // 打开队列(同一线程内，同事只能打开该队列一次)
            mqQueue = mqQueueManager.accessQueue(QUEUE_NAME, openOptions);
            if (mqQueue != null)
                System.out.println("create queue success.");
        } catch (MQException e) {
            System.out.println("create queue failed.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            /*
             * 测试连接
             */
            connectMQ();
            /*
             * 测试创建队列
             */
            testCreateQueue();
            testSendMsg();
            mqQueue.close();
            mqQueueManager.close();
        } catch (MQException e) {
            e.printStackTrace();
        }
    }

    private static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    private static void testSendMsg() {
// 设置发送消息参数为：具有同步性，及支持事务
        MQPutMessageOptions pmo = new MQPutMessageOptions();
        pmo.options = MQConstants.MQPMO_SYNCPOINT;

        // 设置消息格式为字符串类型
        MQMessage msg1 = new MQMessage();
        msg1.format = MQConstants.MQFMT_STRING;
        // 消息内容编码(1208:utf-8)
        msg1.characterSet = 1208;
        try {
            //医院编码
            msg1.setStringProperty("hospital_id", "46600083-8");
            // 消息ID
            msg1.setStringProperty("service_id", "BS004");
            // 就诊类别ID(01 门诊,2 急诊,0201 普通急诊,0202 急诊留观,03 住院,04 体检,0401	普通体检,0402 干保体检,05 转院)
            msg1.setStringProperty("domain_id", "01");
            // 申请科室ID
            msg1.setStringProperty("apply_unit_id", "01010101012304");
            // 发送系统ID
            msg1.setStringProperty("send_sys_id", "S119");
            // 执行科室ID
            msg1.setStringProperty("exec_unit_id", "");
            // 医嘱执行分类编码
            msg1.setStringProperty("order_exec_id", "");
            // 扩展码（empi使用时放入域ID，其它系统标0）
            msg1.setStringProperty("extend_sub_id", "");

            // 消息内容
            String message1 = readFile("bs004_1.xml", StandardCharsets.UTF_8);
            // 设置消息内容
            msg1.writeString(message1);
            // 消息放入隊列
            mqQueue.put(msg1, pmo);
            // 提交事务
            mqQueueManager.commit();
            String str = SimpleStringUtil2.Bytes2HexString(msg1.messageId).toLowerCase();
            System.out.format("BS004 MsgId.length: %s, MsgId: %s\r\n", str.length(), str);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
