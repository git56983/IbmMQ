package com.hisee.main;

import java.util.concurrent.Executors;

import com.hisee.util.BsUtil2;

public class Client {

	public static void main(String[] args) {

		Executors.newFixedThreadPool(5).execute(new Runnable() {

			@Override
			public void run() {
				try {
					BsUtil2.findAllReportsImgAndSendMessage();

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

//		// 因为内部用了while循环，所以分两个线程两跑两种消息
//		Executors.newFixedThreadPool(5).execute(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					BsUtil2.findAllReportsImgSignAndSendMessage();
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		});

	}

}
