package com.hisee.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.UUID;

import com.hisee.bean.PhotoBean;

public class FileUtil {

	/** 
     * 文本文件转换为指定编码的字符串 
     * 
     * @param file         文本文件 
     * @param encoding 编码类型 
     * @return 转换后的字符串 
     * @throws IOException 
     */ 
    public static String file2String(File file, String encoding) { 
            InputStreamReader reader = null; 
            StringWriter writer = new StringWriter(); 
            try { 
                    if (encoding == null || "".equals(encoding.trim())) { 
                            reader = new InputStreamReader(new FileInputStream(file), encoding); 
                    } else { 
                            reader = new InputStreamReader(new FileInputStream(file)); 
                    } 
                    //将输入流写入输出流 
                    char[] buffer = new char[1024]; 
                    int n = 0; 
                    while (-1 != (n = reader.read(buffer))) { 
                            writer.write(buffer, 0, n); 
                    } 
            } catch (Exception e) { 
                    e.printStackTrace(); 
                    return null; 
            } finally { 
                    if (reader != null) 
                            try { 
                                    reader.close(); 
                            } catch (IOException e) { 
                                    e.printStackTrace(); 
                            } 
            } 
            //返回转换结果 
            if (writer != null) 
                    return writer.toString(); 
            else return null; 
    }
    
    
    /**
     *  zh new
	 * 替换动态心电报告xml文本 
	 * 1就诊卡号 
	 * 2住院号 
	 * 3床位 
	 * 4患者姓名 
	 * 5患者性别 
	 * 6患者生日 
	 * 7报告时间 
	 * 8报告医生 
	 * 9审核者时间 
	 * 10审核者 
	 * 11医嘱号 
	 * 12患者年龄
	 */
	public static String replaceByXMLPhoto(String content, PhotoBean pm) {
		content = content.replace("sOutHosCard", pm.getHosNo())
				.replace("sOutHosNo", pm.getVisitNo())
				.replace("sOutBed", pm.getBed())
				.replace("sOutName", pm.getpName())
				.replace("sOutSex", pm.getpSex())
				.replace("sOutBir", pm.getpBir())
				.replace("sOutRepDay", TypeConvert.getNoLineTime(pm.getRepDay()))
				.replace("sOutRepDoc", pm.getRepDoc())
				.replace("sOUTApplyTime", TypeConvert.getNoLineTime(pm.getApplyTime()))
				.replace("sOutApplyer", pm.getApplyer())
				.replace("sOutHisOrdNum", pm.getHisOrdNum())
				.replace("sOutAge", pm.getpAge())
				.replace("sOutDate", pm.getDate())
				.replace("sOutBase64", pm.getPicStr())
				.replace("c5501488-2699-4677-bc8f-2837fc4f33d6", UUID.randomUUID().toString())
				.replace("1314520", pm.getLsNo())
				
				.replace("kdysxm", pm.getKdysxm())
				.replace("kdysgh", pm.getKdysgh())
				.replace("yztjbm", pm.getYztjbm())
				.replace("yztjmc", pm.getYztjmc())
				.replace("applytime", pm.getApplytime())
				.replace("zxksmc", pm.getZxksmc())
				.replace("zxksbm", pm.getZxksbm())
				;
		return content;
	}
}
