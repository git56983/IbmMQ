package com.hisee.util;

import java.util.HashMap;
import java.util.Map;

public class TypeConvert {

	public static void main(String[] args) {

		Map<String, String> result = getStatus(1);
		System.out.println(result);

	}

	/**
	 * 160.003 检查已完成 170.003 检查报告已审核 140.002 检查已到检
	 */
	public static Map<String, String> getStatus(int mycode) {
		Map<String, String> codeV = new HashMap<String, String>();
		if (mycode == 1) {
			codeV.put("140.002", "检查已到检");
		} else if (mycode == 2) {
			codeV.put("160.003", "检查已完成");
		} else if (mycode == 3) {
			codeV.put("170.003", "检查报告已审核");
		}

		return codeV;
	}

	/**
	 * 操作时间
	 */
	public static String getOperTime(int mycode, String bindTime, String unBindTime) {
		if (mycode == 1) {
			return bindTime;
		} else if (mycode == 2) {
			return unBindTime;
		}

		return "";
	}
	
	
	/**
	 * 修改时间格式 2018-11-09 22:11:11 改成20181109221111
	 */
	public static String getNoLineTime(String date) {
		
		return date.replace("-", "").replace(":", "").replace(" ", "");
	}
}
