//package com.hisee.util;
//
//import java.io.ByteArrayInputStream;
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.commons.io.FileUtils;
//import org.dom4j.Document;
//import org.dom4j.DocumentException;
//import org.dom4j.Element;
//import org.dom4j.io.SAXReader;
//import org.joda.time.DateTime;
//
//import com.hisee.bean.BSImgBean;
//import com.hisee.bean.PhotoBean;
//import com.hisee.writedb.WriteMysql;
//import com.ibm.mq.MQMessage;
//import com.ibm.mq.MQPutMessageOptions;
//import com.ibm.mq.MQQueue;
//import com.ibm.mq.MQQueueManager;
//import com.ibm.mq.constants.MQConstants;
//
///**
// * 业务类 zh new
// * 
// */
//public class BsUtil {
//
//	public static void main(String[] args) throws IOException, DocumentException {
//
//		String content = FileUtils.readFileToString(new File("xml.txt"));
//		Map<String, String> m = getMessageHeaderProperties(content);
//
//		// 住院号
//		String hosNo = m.get("hosNo").toString();
//		// 就诊号
//		String visitNo = m.get("visitNo").toString();
//		// 流水号
//		String lsNo = m.get("lsNo").toString();
//
//		String date = "2018-11-21 14:27:24";
//	}
//
//	private static Document getDocumentByString(String content) throws DocumentException {
//		SAXReader reader = new SAXReader();
//		InputStream is = new ByteArrayInputStream(content.getBytes());
//		Document document = reader.read(is);
//		return document;
//	}
//
//	private static Map<String, String> getMessageHeaderProperties(String request_data) throws DocumentException {
//		Map<String, String> p = new HashMap<String, String>();
//
//		Document document = getDocumentByString(request_data);
//		Element root = document.getRootElement();
//		Element placerGroup = root.element("controlActProcess").element("subject").element("placerGroup");
//
//		Iterator<Element> domain_id = placerGroup.element("subject").element("patient").element("id")
//				.elementIterator("item");
//		domain_id.next();
//		String hosNo = domain_id.next().attributeValue("extension");
//		String visitNo = domain_id.next().attributeValue("extension");
//
//		Iterator<Element> ls = placerGroup.element("componentOf1").element("encounter").element("id")
//				.elementIterator("item");
//		ls.next();
//		String lsNo = ls.next().attributeValue("extension");
//
//		// 检查申请时间
//		String applytime = placerGroup.element("component2").element("observationRequest").element("effectiveTime")
//				.element("any").attributeValue("value");
//
////		执行科室编码
//		String zxksbm = placerGroup.element("component2").element("observationRequest").element("location")
//				.element("serviceDeliveryLocation").element("serviceProviderOrganization").element("id").element("item")
//				.attributeValue("extension");
//
////		执行科室名称
//		String zxksmc = placerGroup.element("component2").element("observationRequest").element("location")
//				.element("serviceDeliveryLocation").element("serviceProviderOrganization").element("name")
//				.element("item").element("part").attributeValue("value");
//
////		医嘱组套编码
////		String yztjbm = placerGroup.element("component2").element("observationRequest").element("component2").element("observationRequest").element("code").attributeValue("code");
//		String yztjbm = "5405201901070001";
//
////		医嘱组套名称
////		String yztjmc = placerGroup.element("component2").element("observationRequest").element("component2").element("observationRequest").element("code").element("displayName").attributeValue("value");
//
//		String yztjmc = "动态心电图监测组套";
//
////		医生工号
//		String kdysgh = placerGroup.element("author").element("assignedEntity").element("id").element("item")
//				.attributeValue("extension");
//
////		医生姓名	
//		String kdysxm = placerGroup.element("author").element("assignedEntity").element("assignedPerson")
//				.element("name").element("item").element("part").attributeValue("value");
//
//		// 就诊流水号
//
//		p.put("hosNo", hosNo);
//		p.put("visitNo", visitNo);
//		p.put("lsNo", lsNo);
//
//		p.put("applytime", applytime);
//		p.put("zxksbm", zxksbm);
//		p.put("zxksmc", zxksmc);
//		p.put("yztjbm", yztjbm);
//		p.put("yztjmc", yztjmc);
//
//		p.put("kdysxm", kdysxm);
//
//		p.put("kdysgh", kdysgh);
//
//		System.out.println("===============================" + p);
//		return p;
//	}
//
//	/**
//	 * 发送原始的影像消息
//	 * 
//	 * @param sendQueueManager
//	 * @param sendQueue
//	 * @throws Exception
//	 */
//	public static void findAllReportsImgAndSendMessageDel(MQQueueManager sendQueueManager, MQQueue sendQueue)
//			throws Exception {
//		List<BSImgBean> list = DbUtil.findAllReportImgToSent();
//
//		if (list.size() > 0) {
//			System.out.format("%s: BSIMG list size: %s\r\n",
//					new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime()), list.size());
//			for (int i = 0; i < list.size(); i++) {
//				BSImgBean bSImg = list.get(i);
//
//				String result = BzUtil.downloadFile(bSImg.getFilekey());
//				// reqData check_interface 中的原始数据
//				String reqData = bSImg.getRequestData();
//
//				Map<String, String> m = getMessageHeaderProperties(reqData);
//
//				// 住院号
//				String hosNo = m.get("hosNo").toString();
//				// 就诊号
//				String visitNo = m.get("visitNo").toString();
//				// 流水号
//				String lsNo = m.get("lsNo").toString();
//				// 检查申请时间
//				String applytime = m.get("applytime").toString();
//
//				// 执行科室编码
//				String zxksbm = m.get("zxksbm").toString();
//
//				// 执行科室名称
//				String zxksmc = m.get("zxksmc").toString();
//
//				// 医嘱组套编码
//				String yztjbm = m.get("yztjbm").toString();
//
//				// 医嘱组套名称
//				String yztjmc = m.get("yztjmc").toString();
//
//				// 医生姓名
//				String kdysxm = m.get("kdysxm").toString();
//
//				// 医生工号
//				String kdysgh = m.get("kdysgh").toString();
//
////				PhotoBean pm = new PhotoBean(DateTime.now().toString("yyyyMMddHHmm"), bSImg.getVisitNo(), "0007000178",
////						bSImg.getBedCode(), bSImg.getPatientName(), bSImg.getSex(), "", "", bSImg.getCcTime(),
////						bSImg.getaDocName(), bSImg.getrTime(), bSImg.getrDocName(), bSImg.getDocAdviceId(), result,"0974646","010100032018122000001");
//
//				PhotoBean pm = new PhotoBean(DateTime.now().toString("yyyyMMddHHmm"), bSImg.getVisitNo(), hosNo,
//						bSImg.getBedCode(), bSImg.getPatientName(), bSImg.getSex(), "", "", bSImg.getCcTime(),
//						bSImg.getaDocName(), bSImg.getrTime(), bSImg.getrDocName(), bSImg.getDocAdviceId(), result,
//						visitNo, lsNo, applytime, zxksbm, zxksmc, yztjbm, yztjmc, kdysxm, kdysgh);
//
//				String message = FileUtil.replaceByXMLPhoto(FileUtils.readFileToString(new File("13815948.xml")), pm);
//
//				Map<String, String> p = BzUtil.getMessageHeaderProperties(bSImg.getRequestData());
//
//				System.out.format("%s: Send BSIMG message seq %s begin!\r\n",
//						new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime()), i);
//
//				// 设置发送消息参数为：具有同步性，及支持事务
//				MQPutMessageOptions pmo = new MQPutMessageOptions();
//				pmo.options = MQConstants.MQPMO_SYNCPOINT;
//
//				// 设置消息格式为字符串类型
//				MQMessage msg = new MQMessage();
//				msg.format = MQConstants.MQFMT_STRING;
//
//				// 消息内容编码(1208:utf-8)
//				msg.characterSet = 1208;
//				// 就诊类别domain_id, 申请科室ID apply_unit_id, 执行科室ID exec_unit_id
//				BzUtil.setMessageHeaderProperties(msg, "46600083-8", "BS369", p.get("domain_id"),
//						p.get("apply_unit_id"), "S119", p.get("exec_unit_id"), "0", "0");
//				// 设置消息内容
//				msg.writeString(message);
//				// 消息放入隊列
//				sendQueue.put(msg, pmo);
//
//				//
//				WriteMysql.updateImgStatusByS3Key(bSImg.getcResultId(), 1);
//				// 提交事务
//				sendQueueManager.commit();
//
//				String str = SimpleStringUtil2.Bytes2HexString(msg.messageId).toLowerCase();
//
//				System.out.format("BSIMG MsgId.length: %s, MsgId: %s\r\n", str.length(), str);
//				BzUtil.dumpMsg(message, "BSIMG",
//						new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(Calendar.getInstance().getTime()));
//				Thread.sleep(25);
//			}
//		}
//
//	}
//
//	/**
//	 * 发送已签名的影像消息 在consultation_result表里查询
//	 * 
//	 * @param sendQueueManager
//	 * @param sendQueue
//	 * @throws Exception
//	 */
//	public static void findAllReportsImgSignAndSendMessageDel(MQQueueManager sendQueueManager, MQQueue sendQueue)
//			throws Exception {
//		List<BSImgBean> list = DbUtil.findAllReportImgSignToSent();
//
//		if (list.size() > 0) {
//			System.out.format("%s: findAllReportsImgSignAndSendMessage list size: %s\r\n",
//					new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime()), list.size());
//			for (int i = 0; i < list.size(); i++) {
//				BSImgBean bSImg = list.get(i);
//
//				String result = BzUtil.downloadFile(bSImg.getFilekey());
//
//				String reqData = bSImg.getRequestData();
//
//				Map<String, String> m = getMessageHeaderProperties(reqData);
//
//				// 住院号
//				String hosNo = m.get("hosNo").toString();
//				// 就诊号
//				String visitNo = m.get("visitNo").toString();
//				// 流水号
//				String lsNo = m.get("lsNo").toString();
//
//				// 检查申请时间
//				String applytime = m.get("applytime").toString();
//
//				// 执行科室编码
//				String zxksbm = m.get("zxksbm").toString();
//
//				// 执行科室名称
//				String zxksmc = m.get("zxksmc").toString();
//
//				// 医嘱组套编码
//				String yztjbm = m.get("yztjbm").toString();
//
//				// 医嘱组套名称
//				String yztjmc = m.get("yztjmc").toString();
//
//				// 医生姓名
//				String kdysxm = m.get("kdysxm").toString();
//
//				// 医生工号
//				String kdysgh = m.get("kdysgh").toString();
//
//				PhotoBean pm = new PhotoBean(DateTime.now().toString("yyyyMMddHHmm"), bSImg.getVisitNo(), hosNo,
//						bSImg.getBedCode(), bSImg.getPatientName(), bSImg.getSex(), "", "", bSImg.getCcTime(),
//						bSImg.getaDocName(), bSImg.getrTime(), bSImg.getrDocName(), bSImg.getDocAdviceId(), result,
//						visitNo, lsNo, applytime, zxksbm, zxksmc, yztjbm, yztjmc, kdysxm, kdysgh);
//
//				String message = FileUtil.replaceByXMLPhoto(FileUtils.readFileToString(new File("13815948_sign.xml")),
//						pm);
//
//				Map<String, String> p = BzUtil.getMessageHeaderProperties(bSImg.getRequestData());
//
//				System.out.format("%s: Send findAllReportsImgSignAndSendMessage message seq %s begin!\r\n",
//						new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime()), i);
//
//				// 设置发送消息参数为：具有同步性，及支持事务
//				MQPutMessageOptions pmo = new MQPutMessageOptions();
//				pmo.options = MQConstants.MQPMO_SYNCPOINT;
//
//				// 设置消息格式为字符串类型
//				MQMessage msg = new MQMessage();
//				msg.format = MQConstants.MQFMT_STRING;
//
//				// 消息内容编码(1208:utf-8)
//				msg.characterSet = 1208;
//				// 就诊类别domain_id, 申请科室ID apply_unit_id, 执行科室ID exec_unit_id
//				BzUtil.setMessageHeaderProperties(msg, "46600083-8", "BS369", p.get("domain_id"),
//						p.get("apply_unit_id"), "S119", p.get("exec_unit_id"), "0", "0");
//				// 设置消息内容
//				msg.writeString(message);
//				// 消息放入隊列
//				sendQueue.put(msg, pmo);
//
//				//
//				WriteMysql.updateImgStatusByS3Key(bSImg.getcResultId(), 2);
//				// 提交事务
//				sendQueueManager.commit();
//
//				String str = SimpleStringUtil2.Bytes2HexString(msg.messageId).toLowerCase();
//
//				System.out.format("findAllReportsImgSignAndSendMessage MsgId.length: %s, MsgId: %s\r\n", str.length(),
//						str);
//				BzUtil.dumpMsg(message, "BSIMG",
//						new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(Calendar.getInstance().getTime()));
//				Thread.sleep(25);
//			}
//		}
//
//	}
//
//	/**
//	 * 发送原始的影像消息
//	 * 
//	 * @param sendQueueManager
//	 * @param sendQueue
//	 * @throws Exception
//	 */
//	public static void findAllReportsImgAndSendMessage(MQMessage msg, MQQueue queue, MQQueueManager queueManager,
//			MQPutMessageOptions pmo) throws Exception {
//
//		while (true) {
//			List<BSImgBean> list = DbUtil.findAllReportImgToSent();
//
//			if (list.size() > 0) {
//				System.out.format("%s: BSIMG list size: %s\r\n",
//						new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime()),
//						list.size());
//				for (int i = 0; i < list.size(); i++) {
//					BSImgBean bSImg = list.get(i);
//
//					String result = BzUtil.downloadFile(bSImg.getFilekey());
//					// reqData check_interface 中的原始数据
//					String reqData = bSImg.getRequestData();
//
//					Map<String, String> m = getMessageHeaderProperties(reqData);
//
//					// 住院号
//					String hosNo = m.get("hosNo").toString();
//					// 就诊号
//					String visitNo = m.get("visitNo").toString();
//					// 流水号
//					String lsNo = m.get("lsNo").toString();
//					// 检查申请时间
//					String applytime = m.get("applytime").toString();
//
//					// 执行科室编码
//					String zxksbm = m.get("zxksbm").toString();
//
//					// 执行科室名称
//					String zxksmc = m.get("zxksmc").toString();
//
//					// 医嘱组套编码
//					String yztjbm = m.get("yztjbm").toString();
//
//					// 医嘱组套名称
//					String yztjmc = m.get("yztjmc").toString();
//
//					// 医生姓名
//					String kdysxm = m.get("kdysxm").toString();
//
//					// 医生工号
//					String kdysgh = m.get("kdysgh").toString();
//
//					PhotoBean pm = new PhotoBean(DateTime.now().toString("yyyyMMddHHmm"), bSImg.getVisitNo(), hosNo,
//							bSImg.getBedCode(), bSImg.getPatientName(), bSImg.getSex(), "", "", bSImg.getCcTime(),
//							bSImg.getaDocName(), bSImg.getrTime(), bSImg.getrDocName(), bSImg.getDocAdviceId(), result,
//							visitNo, lsNo, applytime, zxksbm, zxksmc, yztjbm, yztjmc, kdysxm, kdysgh);
//
//					String message = FileUtil.replaceByXMLPhoto(FileUtils.readFileToString(new File("13815948.xml")),
//							pm);
//
//					Map<String, String> p = BzUtil.getMessageHeaderProperties(bSImg.getRequestData());
//
//					System.out.format("%s: Send BSIMG message seq %s begin!\r\n",
//							new SimpleDateFormat("yyyyMMdd_HHmmss.SSS").format(Calendar.getInstance().getTime()), i);
//
//					Thread.sleep(50);
//
//					WriteMysql.updateImgStatusByS3Key(bSImg.getcResultId(), 1);
//
//					BzUtil.dumpMsg(message, "BSIMG",
//							new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(Calendar.getInstance().getTime()));
//
//					// 设置消息内容
//					msg.writeString(message);
//					// 消息放入隊列
//					queue.put(msg, pmo);
//
//					// 提交事务
//					queueManager.commit();
//					System.out.println("发送成功！");
//
//					String str = SimpleStringUtil2.Bytes2HexString(msg.messageId).toLowerCase();
//					System.out.println("MsgId:" + str.length() + "~~~~MsgId:" + str);
//
//				}
//			} else {
//				System.out.format("%s:list =0 ");
//			}
//		}
//
//	}
//
//}
