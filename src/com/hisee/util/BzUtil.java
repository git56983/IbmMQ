package com.hisee.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.ibm.mq.MQException;
import com.ibm.mq.MQMessage;

import io.minio.MinioClient;
import sun.misc.BASE64Encoder;

/**
 * 工具类 zh new
 * 
 */
public class BzUtil {

	// 保存的消息文件放到msg目录
	public static final String MSG_DUMP_FOLDER = "msg/";
	public static final String MSG_DUMP_FILENAME = "%s%s%s.xml";
	public static final String FILE_NAME = "temp.pdf";

	public static final String MINIO_SERVER_ADDR = "http://10.82.163.48:9000";

	public static final String ACCESS_KEY = "08UX1N6CE2PZ9YILL8BJ";
	public static final String SECRET_KEY = "ltvQpnMF7sfv7i6KbKB+JkFcGnPNFs9rs+qtTRv8";
	public static final String BUCKET_NAME = "remoteecg";

	public static void dumpMsg(String message, String msg_type, String recordTimeStamp) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter(
				String.format(MSG_DUMP_FILENAME, MSG_DUMP_FOLDER, msg_type, recordTimeStamp))) {
			out.println(message);
		}
	}

	/**
	 * 设置消息头
	 * 
	 * @param msg
	 * @param hospital_id
	 * @param service_id
	 * @param domain_id
	 * @param apply_unit_id
	 * @param send_sys_id
	 * @param exec_unit_id
	 * @param order_exec_id 固定为0
	 * @param extend_sub_id 固定为0
	 * @throws MQException
	 */
	public static void setMessageHeaderProperties(MQMessage msg, String hospital_id, String service_id,
			String domain_id, String apply_unit_id, String send_sys_id, String exec_unit_id, String order_exec_id,
			String extend_sub_id) throws MQException {
//医院编码
		System.out.println("hospital_id====================================================>" + hospital_id);
		msg.setStringProperty("hospital_id", hospital_id);
// 消息ID
		System.out.println("service_id================================>" + service_id);
		msg.setStringProperty("service_id", service_id);
// 就诊类别ID(01 门诊,2 急诊,0201 普通急诊,0202 急诊留观,03 住院,04 体检,0401	普通体检,0402 干保体检,05 转院)

		System.out.println("domain_id================================>" + domain_id);
		msg.setStringProperty("domain_id", domain_id);
// 申请科室ID
		System.out.println("apply_unit_id================================>" + apply_unit_id);
		msg.setStringProperty("apply_unit_id", apply_unit_id);
// 发送系统ID
		System.out.println("send_sys_id================================>" + send_sys_id);
		msg.setStringProperty("send_sys_id", send_sys_id);
// 执行科室ID
		System.out.println("exec_unit_id================================>" + exec_unit_id);
		msg.setStringProperty("exec_unit_id", exec_unit_id);
// 医嘱执行分类编码
		System.out.println("order_exec_id================================>" + order_exec_id);
		msg.setStringProperty("order_exec_id", order_exec_id);
// 扩展码（empi使用时放入域ID，其它系统标0）
		System.out.println("extend_sub_id================================>" + extend_sub_id);
		msg.setStringProperty("extend_sub_id", extend_sub_id);

	}
	
	
	
	
	
	
	/**
	 * 设置消息头
	 * 
	 * @param msg
	 * @param hospital_id
	 * @param service_id
	 * @param domain_id
	 * @param apply_unit_id
	 * @param send_sys_id
	 * @param exec_unit_id
	 * @param order_exec_id 固定为0
	 * @param extend_sub_id 固定为0
	 * @throws MQException
	 */
	public static void setMessageHeaderProperties2(MQMessage msg, String hospital_id, String service_id,
			String domain_id, String apply_unit_id, String send_sys_id, String exec_unit_id, String order_exec_id,
			String extend_sub_id) throws MQException {
		
				
		msg.setStringProperty("hospital_id", "46600083-8");
		msg.setStringProperty("service_id", "BS369");

		msg.setStringProperty("domain_id", "03");
		msg.setStringProperty("apply_unit_id", "010101010117");
		msg.setStringProperty("send_sys_id", "S119");
		msg.setStringProperty("exec_unit_id", "010101010117");
		msg.setStringProperty("order_exec_id", "0");
		msg.setStringProperty("extend_sub_id", "0");

	}

	public static Map<String, String> getMessageHeaderProperties(String request_data) throws DocumentException {
		Map<String, String> p = new HashMap<String, String>();

		Document document = getDocumentByString(request_data);
		Element root = document.getRootElement();
		// examination_code 申请id
		Element placerGroup = root.element("controlActProcess").element("subject").element("placerGroup");
		String domain_id = placerGroup.element("componentOf1").element("encounter").element("code")
				.attributeValue("code");

		String apply_unit_id = placerGroup.element("author").element("assignedEntity")
				.element("representedOrganization").element("id").element("item").attributeValue("extension");

		String exec_unit_id = placerGroup.element("component2").element("observationRequest").element("location")
				.element("serviceDeliveryLocation").element("serviceProviderOrganization").element("id").element("item")
				.attributeValue("extension");

		// 就诊类别ID(01 门诊,2 急诊,0201 普通急诊,0202 急诊留观,03 住院,04 体检,0401 普通体检,0402 干保体检,05 转院)
		p.put("domain_id", domain_id);
		System.out.format("domain_id: %s\r\n", domain_id);
		// 申请科室ID
		p.put("apply_unit_id", apply_unit_id);
		System.out.format("apply_unit_id: %s\r\n", apply_unit_id);
		// 执行科室ID
		p.put("exec_unit_id", exec_unit_id);
		System.out.format("exec_unit_id: %s\r\n", exec_unit_id);

		return p;
	}

	public static Document getDocumentByString(String content) throws DocumentException {
		SAXReader reader = new SAXReader();
		InputStream is = new ByteArrayInputStream(content.getBytes());
		Document document = reader.read(is);
		return document;
	}

	/**
	 *
	 * @param key
	 * @return
	 */
	public static String downloadFile(String key) {
		try {

			File file = new File(FILE_NAME);
			file.delete();

			MinioClient minioClient = new MinioClient(MINIO_SERVER_ADDR, ACCESS_KEY, SECRET_KEY);

			minioClient.statObject(BUCKET_NAME, key);// 9c08203493e5edb73794257b2505ae80
			System.out.format("MINIO_SERVER_ADDR: %s, FILE_NAME: %s\r\n", MINIO_SERVER_ADDR, FILE_NAME);
			minioClient.getObject(BUCKET_NAME, key, FILE_NAME); // "14bc898909312cb0fee3df928362ecfb"
			System.out.println("File downloaded");

			String result = encodeBase64File(FILE_NAME);
			return result;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";

	}

	/**
	 *
	 * @param path 文件路径
	 * @return BASE64编码后的字符串
	 * @throws Exception 抛出异常
	 */
	public static String encodeBase64File(String path) throws Exception {
		File file = new File(path);
		FileInputStream inputFile = new FileInputStream(file);
		byte[] buffer = new byte[(int) file.length()];
		inputFile.read(buffer);
		inputFile.close();
		return new BASE64Encoder().encode(buffer);
	}
}
