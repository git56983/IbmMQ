package com.hisee.bean;

import java.io.Serializable;

/**
 * zh new
 */
public class BSImgBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 就诊号 */
	private String visitNo;

	/** 床号 */
	private String bedCode;

	/** 患者姓名 */
	private String patientName;

	/** 患者性别 */
	private String sex;

	/** 患者年龄 */
	private String age;

	/** 诊断时间 */
	private String ccTime;

	/** 诊断医生 */
	private String aDocName;

	/** 审核医生 */
	private String rDocName;

	/** 审核时间 */
	private String rTime;

	/** 文件key */
	private String filekey;

	/** 医嘱号 */
	private String docAdviceId;

	/** 请求数据 */
	private String requestData;

	/** 主键 */
	private String cResultId;

	public String getcResultId() {
		return cResultId;
	}

	public void setcResultId(String cResultId) {
		this.cResultId = cResultId;
	}

	public String getRequestData() {
		return requestData;
	}

	public void setRequestData(String requestData) {
		this.requestData = requestData;
	}

	public String getaDocName() {
		return aDocName;
	}

	public String getAge() {
		return age;
	}

	public String getBedCode() {
		return bedCode;
	}

	public String getCcTime() {
		return ccTime;
	}

	public String getDocAdviceId() {
		return docAdviceId;
	}

	public String getFilekey() {
		return filekey;
	}

	public String getPatientName() {
		return patientName;
	}

	public String getrDocName() {
		return rDocName;
	}

	public String getrTime() {
		return rTime;
	}

	public String getSex() {
		return sex;
	}

	public String getVisitNo() {
		return visitNo;
	}

	public void setaDocName(String aDocName) {
		this.aDocName = aDocName;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public void setBedCode(String bedCode) {
		this.bedCode = bedCode;
	}

	public void setCcTime(String ccTime) {
		this.ccTime = ccTime;
	}

	public void setDocAdviceId(String docAdviceId) {
		this.docAdviceId = docAdviceId;
	}

	public void setFilekey(String filekey) {
		this.filekey = filekey;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public void setrDocName(String rDocName) {
		this.rDocName = rDocName;
	}

	public void setrTime(String rTime) {
		this.rTime = rTime;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

}
