package com.hisee.bean;

import java.io.Serializable;

public class BS320 implements Serializable {

	private static final long serialVersionUID = 1L;

	// 患者ID
	private String patientCode;

	// 就诊号
	private String visitNo;

	// pdf转base64
	private String base64;

	private String fileS3Key;
	
	//文档生效时间
	private String effectiveTime;

	private String requestData;

	public String getEffectiveTime() {
		return effectiveTime;
	}

	public void setEffectiveTime(String effectiveTime) {
		this.effectiveTime = effectiveTime;
	}

	public String getFileS3Key() {
		return fileS3Key;
	}

	public void setFileS3Key(String fileS3Key) {
		this.fileS3Key = fileS3Key;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getRequestData() { return requestData; }

	public void setRequestData(String requestData) { this.requestData = requestData; }

}
