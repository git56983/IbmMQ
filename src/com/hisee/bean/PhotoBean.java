package com.hisee.bean;

/**
 * zh new
 */
public class PhotoBean implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 检查申请时间
	private String applytime;
//	执行科室编码
	private String zxksbm;
//	执行科室名称
	private String zxksmc;
//	医嘱组套编码
	private String yztjbm;
//	医嘱组套名称
	private String yztjmc;
//	医生姓名
	private String kdysxm;
//	医生工号
	private String kdysgh;

	// 文档生成时间
	private String date = "";

	// 就诊卡号
	private String cardNo = "";

	// 住院号
	private String hosNo = "";

	private String visitNo = "";

	private String lsNo = "";

	// 床位
	private String bed = "";

	// 患者姓名
	private String pName = "";

	// 患者性别
	private String pSex = "";

	// 患者年龄
	private String pAge = "";

	// 患者生日
	private String pBir = "";

	// 报告时间
	private String repDay = "";

	// 报告医生
	private String repDoc = "";

	// 审核者时间
	private String applyTime = "";

	// 审核者
	private String applyer = "";

	// 医嘱号
	private String hisOrdNum = "";

// 图片字符串
	private String picStr = "";

	public PhotoBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * cardNo hosNo 从check_interface里取visit_no 是根据就诊类型来判断是否是填充就诊卡或住院号的 就诊类别=01
	 * 门诊时为就诊卡号 就诊类别=0201 急诊为就诊卡号 就诊类别=03住院时为住院号 就诊类别=0401普通体检时为体验档案号
	 * 就诊类别=0402干保体检时为体检档案号 <!--就诊类别编码-->
	 * <code codeSystem="1.2.156.112635.1.1.80" code="03">
	     <!-- 就诊类别名称 -->
	     <displayName value="住院" />
	 </code>
	 * 
	 * @param date
	 * @param cardNo
	 * @param hosNo
	 * @param bed
	 * @param pName
	 * @param pSex
	 * @param pAge
	 * @param pBir
	 * @param repDay
	 * @param repDoc
	 * @param applyTime
	 * @param applyer
	 * @param hisOrdNum
	 * @param picStr
	 */
	public PhotoBean(String date, String cardNo, String hosNo, String bed, String pName, String pSex, String pAge,
			String pBir, String repDay, String repDoc, String applyTime, String applyer, String hisOrdNum,
			String picStr, String visitNo, String lsNo, String applytime, String zxksbm, String zxksmc, String yztjbm,
			String yztjmc, String kdysxm, String kdysgh) {
		super();
		this.date = date;
		this.cardNo = cardNo;
		this.hosNo = hosNo;
		this.bed = bed;
		this.pName = pName;
		this.pSex = pSex;
		this.pAge = pAge;
		this.pBir = pBir;
		this.repDay = repDay;
		this.repDoc = repDoc;
		this.applyTime = applyTime;
		this.applyer = applyer;
		this.hisOrdNum = hisOrdNum;
		this.picStr = picStr;
		this.visitNo = visitNo;
		this.lsNo = lsNo;

		this.applytime = applytime;
		this.zxksbm = zxksbm;
		this.zxksmc = zxksmc;
		this.yztjbm = yztjbm;
		this.yztjmc = yztjmc;
		this.kdysxm = kdysxm;
		this.kdysgh = kdysgh;
	}

	public String getApplyer() {
		return applyer;
	}

	public String getApplytime() {
		return applytime;
	}

	public String getApplyTime() {
		return applyTime;
	}

	public String getBed() {
		return bed;
	}

	public String getCardNo() {
		return cardNo;
	}

	public String getDate() {
		return date;
	}

	public String getHisOrdNum() {
		return hisOrdNum;
	}

	public String getHosNo() {
		return hosNo;
	}

	public String getKdysgh() {
		return kdysgh;
	}

	public String getKdysxm() {
		return kdysxm;
	}

	public String getLsNo() {
		return lsNo;
	}

	public String getpAge() {
		return pAge;
	}

	public String getpBir() {
		return pBir;
	}

	public String getPicStr() {
		return picStr;
	}

	public String getpName() {
		return pName;
	}

	public String getpSex() {
		return pSex;
	}

	public String getRepDay() {
		return repDay;
	}

	public String getRepDoc() {
		return repDoc;
	}

	public String getVisitNo() {
		return visitNo;
	}

	public String getYztjbm() {
		return yztjbm;
	}

	public String getYztjmc() {
		return yztjmc;
	}

	public String getZxksbm() {
		return zxksbm;
	}

	public String getZxksmc() {
		return zxksmc;
	}

	public void setApplyer(String applyer) {
		this.applyer = applyer;
	}

	public void setApplytime(String applytime) {
		this.applytime = applytime;
	}

	public void setApplyTime(String applyTime) {
		this.applyTime = applyTime;
	}

	public void setBed(String bed) {
		this.bed = bed;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setHisOrdNum(String hisOrdNum) {
		this.hisOrdNum = hisOrdNum;
	}

	public void setHosNo(String hosNo) {
		this.hosNo = hosNo;
	}

	public void setKdysgh(String kdysgh) {
		this.kdysgh = kdysgh;
	}

	public void setKdysxm(String kdysxm) {
		this.kdysxm = kdysxm;
	}

	public void setLsNo(String lsNo) {
		this.lsNo = lsNo;
	}

	public void setpAge(String pAge) {
		this.pAge = pAge;
	}

	public void setpBir(String pBir) {
		this.pBir = pBir;
	}

	public void setPicStr(String picStr) {
		this.picStr = picStr;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public void setpSex(String pSex) {
		this.pSex = pSex;
	}

	public void setRepDay(String repDay) {
		this.repDay = repDay;
	}

	public void setRepDoc(String repDoc) {
		this.repDoc = repDoc;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public void setYztjbm(String yztjbm) {
		this.yztjbm = yztjbm;
	}

	public void setYztjmc(String yztjmc) {
		this.yztjmc = yztjmc;
	}

	public void setZxksbm(String zxksbm) {
		this.zxksbm = zxksbm;
	}

	public void setZxksmc(String zxksmc) {
		this.zxksmc = zxksmc;
	}
}
