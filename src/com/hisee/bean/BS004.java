package com.hisee.bean;

import java.io.Serializable;

public class BS004 implements Serializable {

	private static final long serialVersionUID = 1L;

	// 患者ID  
	private String patientCode;

	// 就诊号
	private String visitNo;

	// 操作时间
	private String operTime;

	// 执行医生编码
	private String operCode;

	// 执行医生姓名
	private String operName;

	// 执行科室编码
	private String ksCode;

	// 执行科室名称
	private String ksName;

	// 医嘱号
	private String doctorAdviceId;

	// 医嘱执行状态
	private String doctorAdviceCode;

	// 医嘱执行名称
	private String doctorAdviceName;

	private String requestData;

	private String status;

	public String getDoctorAdviceCode() {
		return doctorAdviceCode;
	}

	public String getDoctorAdviceId() {
		return doctorAdviceId;
	}

	public String getDoctorAdviceName() {
		return doctorAdviceName;
	}

	public String getKsCode() {
		return ksCode;
	}

	public String getKsName() {
		return ksName;
	}

	public String getOperCode() {
		return operCode;
	}

	public String getOperName() {
		return operName;
	}

	public String getOperTime() {
		return operTime;
	}

	public String getPatientCode() {
		return patientCode;
	}

	public String getVisitNo() {
		return visitNo;
	}

	public String getStatus() { return status; }

	public String getRequestData() { return requestData; }

	public void setDoctorAdviceCode(String doctorAdviceCode) {
		this.doctorAdviceCode = doctorAdviceCode;
	}

	public void setDoctorAdviceId(String doctorAdviceId) {
		this.doctorAdviceId = doctorAdviceId;
	}

	public void setDoctorAdviceName(String doctorAdviceName) {
		this.doctorAdviceName = doctorAdviceName;
	}

	public void setKsCode(String ksCode) {
		this.ksCode = ksCode;
	}

	public void setKsName(String ksName) {
		this.ksName = ksName;
	}

	public void setOperCode(String operCode) {
		this.operCode = operCode;
	}

	public void setOperName(String operName) {
		this.operName = operName;
	}

	public void setOperTime(String operTime) {
		this.operTime = operTime;
	}

	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public void setStatus(String status) { this.status = status; }

	public void setRequestData(String requestData) { this.requestData = requestData; }
}
