package com.hisee.writedb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.hisee.util.DBConnectionPool;
import com.hisee.util.DbUtil;

public class WriteMysql {

	/**
	 * 插入
	 */
	@SuppressWarnings("unused")
	public static void insert(Map<String, Object> map) {
		map.put("insert_time", new Date());
		try {
			int count = DbUtil.insert("check_interface", map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 更新
	 */
	@SuppressWarnings("unused")
	public static void update(String examination_code,String content) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("withdraw", "1");
		map.put("withdraw_request_data", content);
		map.put("update_time", new Date());
		Map<String, Object> whereMap = new HashMap<String, Object>();
		whereMap.put("examination_code", examination_code);
		try {
			int count = DbUtil.update("check_interface", map, whereMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int find(String examination_code) throws Exception {
		/** 影响的行数 **/
		int total = 0;
		String sql = "SELECT count(*) as total FROM check_interface where examination_code = ?";
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			/** 从数据库连接池中获取数据库连接 **/
			connection = DBConnectionPool.getInstance().getConnection();
			/** 执行SQL预编译 **/
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setObject(1, examination_code);
			/** 执行sql **/
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				total = rs.getInt("total");
			}
		} catch (Exception e) {
			if (connection != null) {
				connection.rollback();
			}
			e.printStackTrace();
			throw e;
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		return total;
	}

	public static int findByDoctorAdviceId(String doctor_advice_id) {
		int total = 0;
		try {
			total = DbUtil.findByDoctorAdviceId(doctor_advice_id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return total;
	}

	/**
	 * 推送后改check_interface的mq_status字段
	 */
	public static void updateStatus(String doctorAdviceId, int status) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mq_status", status);
		Map<String, Object> whereMap = new HashMap<String, Object>();
		whereMap.put("doctor_advice_id", doctorAdviceId);
		try {
			DbUtil.update("check_interface", map, whereMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 推送BS320消息后，改check_interface的mq_status字段
	 */
	public static void updateStatusByS3Key(String fileS3Key, int status) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uploaded", status);
		Map<String, Object> whereMap = new HashMap<String, Object>();
		whereMap.put("file_s3key ", fileS3Key);
		try {
			DbUtil.update("upload", map, whereMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * zh new
	 * 推送影像消息后，根据主键修改upload_status字段
	 */
	public static void updateImgStatusByS3Key(String id, int upload_status) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("upload_status", upload_status);
		Map<String, Object> whereMap = new HashMap<String, Object>();
		whereMap.put("cons_result_id ", id);
		try {
			DbUtil.update("consultation_result", map, whereMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
